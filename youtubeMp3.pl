#!/usr/bin/perl
######
#Gui para descargar un video de youtube y codificarlo en mp3
#Input: (opcional) URL del video
#####
use Tk;
use Tk::DialogBox;
use Clipboard;
use strict;
#Directorio de trabajo
use constant DIRTRABAJO=>$ENV{"HOME"};
my $mw;

#####
#Función Pública procesarURL
#Input: URL a descargar
#####
sub procesarURL($)
{
    my $url=$_[0];
    my $titulo=&obtenerVideo($url);
    if ($titulo)
    {
        convertirVideoMp3($titulo);
    }
}

#####
#Función Pública obtenerVideo
#Input: URL del video
#####
sub obtenerVideo($)
{
    chdir DIRTRABAJO;
    my $url=$_[0];
    my $cmd="youtube-dl --ignore-config ".$url;
    my @videoInfo=`$cmd`;
    my $titulo="video.mkv";
    foreach (@videoInfo)
    {
        if ($_ =~ /Destination/)
        {
            $titulo=$_;
            $titulo=~s/^.*\:\ //;
        }
        if ($_ =~  /Merging/)
        {
            $titulo=$_;
            $titulo=~s/^.*\"(.*)\"$/\1/;
            chomp $titulo;
        }

   }
    undef @videoInfo;
    return($titulo);
}

#####
#Función Pública convertirVideoMp3
#Input: Fichero de video a convertir
#####
sub convertirVideoMp3($)
{
    chdir DIRTRABAJO;
    my $video=$_[0];
    my $mp3=$video;
    $mp3=~s/(^\ |\ $)//;
    $mp3=~s/ /_/g;
    $mp3=~s/\.[[:alnum:]]{3}$/\.mp3/;
    system ('ffmpeg -i "'.DIRTRABAJO."/".$video.'" -vn -ar 44100 -ac 2 -ab 192k -f mp3 "'.DIRTRABAJO."/".$mp3.'"');
#    unlink DIRTRABAJO."/".$video;
#    `zenity --info --text="Finalizado"`;
	finish($video);

}

sub finish($)
{
	print("Finished!");
	my $md=$mw->DialogBox(-title=>"Finalizado",-buttons=>["OK"]);
    my $txt_Label=$_[0]." descargado a ".DIRTRABAJO;
    $md->Label(-text=>$txt_Label)->pack;
	$md->Show()
}
sub main($)
{
	$mw=MainWindow->new;
    my $txt_Label="Introduce la url:";
    $mw->Label(-text=>$txt_Label)->pack;
    my $url=$_[0];
    my $inp_URL=$mw->Entry(-textvariable=>\$url)->pack;
    $mw->Button(-text=>"Aceptar",-command=>sub {&procesarURL($url)})->pack(-side=>"right");
    $mw->Button(-text=>"Salir",-command=>sub {exit})->pack(-side=>"left");
    $mw->withdraw;
    $mw->Popup;
    MainLoop;
}

my $url;
if (@ARGV)
{
    $url=shift (@ARGV);
} else {
    eval
    {	
		my $contenidoClipboard=Clipboard->paste;
		if ($contenidoClipboard =~ /^htt(p|ps):\/\// )
		{
			$url=$contenidoClipboard;
		}
		undef $contenidoClipboard;
    }
}

&main ($url);
